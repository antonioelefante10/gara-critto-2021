import re
import math
from termcolor import colored
import os

os.system("clear")
DEPTH_TEST = 10
VALIDA_A_VALUES = [3,5,7,9,11,13,15,17,19,21,23,25]
MINIMUM_EN_IT_WORD_LENGHT = 5

inglese = []
italiano = []
words_score = {}
probable_phrases = []
definitive_phrases = {}
with open('./Dizionari/inglese.txt', encoding='utf8') as f:
	for line in f:
		w = line.strip()
		if len(w) >= MINIMUM_EN_IT_WORD_LENGHT:
			inglese.append(w.upper())

with open('./Dizionari/italiano.txt', encoding='utf8') as f:
	for line in f:
		w = line.strip()
		if len(w) >= MINIMUM_EN_IT_WORD_LENGHT:
			italiano.append(w.upper())


# Implementation of Affine Cipher in Python
  
# Extended Euclidean Algorithm for finding modular inverse
# eg: modinv(7, 26) = 15
def egcd(a, b):
    x,y, u,v = 0,1, 1,0
    while a != 0:
        q, r = b//a, b%a
        m, n = x-u*q, y-v*q
        b,a, x,y, u,v = a,r, u,v, m,n
    gcd = b
    return gcd, x, y
  
def modinv(a, m):
    gcd, x, y = egcd(a, m)
    if gcd != 1:
        return None  # modular inverse does not exist
    else:
        return x % m
  
  
# affine cipher encrytion function 
# returns the cipher text
def affine_encrypt(text, key):
    '''
    C = (a*P + b) % 26
    '''
    return ''.join([ chr((( key[0]*(ord(t) - ord('A')) + key[1] ) % 26) 
                  + ord('A')) for t in text.upper().replace(' ', '') ])
  
  
# affine cipher decryption function 
# returns original text
def affine_decrypt(cipher, key):
    '''
    P = (a^-1 * (C - b)) % 26
    '''
    print("key: ", key)
    return ''.join([ chr((( modinv(key[0], 26)*(ord(c) - ord('A') - key[1])) 
                    % 26) + ord('A')) for c in cipher ])
  
  



ALFABETO_CIFRANTE = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"

def text_formatter(text):
	symbol_less_text = re.sub(r"[^A-Za-z]+", '', text)
	uppercase_text = symbol_less_text.upper()
	return uppercase_text

# il cifrario affine applica una trasformazione c = ax + b dove x è il testo in chiaro
# mentre a e b sono due valori tali da permettere la trasformazione

# rompere tale cifrario è "relativamente" semplice basta effettuare un analisi del testo cifrato
# e osservare le frequenze dei caratteri


def order_dict(x):
	return {k: v for k, v in sorted(x.items(), key=lambda item: item[1],reverse=True)}

def test_substitutions(text, english):
	global probable_phrases
	text = text_formatter(text)
	def frequence_test(text):
		frequence_test = {}
		for c in ALFABETO_CIFRANTE:
			frequence_test[c] = 0

		for c in text:
			frequence_test[c] += 1

		return order_dict(frequence_test)
	
	def affine_decipher(text,english,a,b):
		def extract_coefficients(english,a,b):
			idx_a = ALFABETO_CIFRANTE.index(a)
			idx_b = ALFABETO_CIFRANTE.index(b)

			# devo estrarre i coefficienti a e b
			# dalla equazione:
			#
			#	?inglese?
			#	{ 4 * a + b = idx_a
			#	{ 19 * a + b = idx_b
			#
			#	?italiano?
			#	{ 4 * a + b = idx_a
			#	{ 0 * a + b = idx_b
			#
			# e successivamente valutare se gdc(a,26) = 1
			a = 0
			b = 0
			if english:
				a = (idx_b - idx_a) / 15
				b = idx_a - 4 * a
			else:
				a = (idx_a - idx_b) / 4
				b = idx_b

			return (abs(int(a)),abs(int(b)))


		def find_a_b_affine(Idx1, Idx2, freq_idx1, freq_idx2, modulo_value):
			def modular_inverse(value):
				return pow(value, -1, 26)
			res_a = (((Idx1 - Idx2) % modulo_value) * modular_inverse((freq_idx1 - freq_idx2))) % modulo_value
			res_b = (Idx1 - (res_a * freq_idx1)) % modulo_value
			return (res_a,res_b)

		a_coeff, b_coeff = (0,0)
		idx_a = ALFABETO_CIFRANTE.index(a)
		idx_b = ALFABETO_CIFRANTE.index(b)
		try:
			if english:
				(a_coeff,b_coeff) = find_a_b_affine(Idx1=idx_a,Idx2=idx_b, freq_idx1=4, freq_idx2=19, modulo_value=26)
			else:
				(a_coeff,b_coeff) = find_a_b_affine(Idx1=idx_a,Idx2=idx_b, freq_idx1=4, freq_idx2=0, modulo_value=26)
		except Exception as e:
			print(e)
			return

		if math.gcd(a_coeff,26) != 1:
			return
		else:
			if english:
				print("e: " + str(a_coeff))
				print("t: " + str(b_coeff))
			else:
				print("a: " + str(a_coeff))
				print("e: " + str(b_coeff))
		phrase = affine_decrypt(text, [a_coeff,b_coeff])
		print(colored(phrase,"yellow"))
		probable_phrases.append(phrase)
	
	result_frequence_test = frequence_test(text)
	keys = list(result_frequence_test.keys())
	for i in range(DEPTH_TEST):
		for j in range(DEPTH_TEST):
			# prima cosa controllo che non sto testando lo stesso set di lettere
			if i == j:
				continue
			affine_decipher(text, english, keys[i], keys[j])
	

def main():
	enc_text = """
	awvvjihqvlypvmfdfyvrlnvydtrvplrlevatdhppfaavvwlr
uvvyivsfvbfynsvitilivdlrvrhejyjrjlgughhmdgharfy
qvhqgvslddfylavmbfawawvhkehimlrailcvyvdlslddfyv

farlfmawlaawvdhppfaavvwlmyhatvaivldwvml
dhydgjrfhylymawvivsfvbfrdjiivyagthynhfynujafafr
vkqvdavmahlyyhjydvefymfynrhybvmyvrmlthiawjirmlt

awvjxivnjglahiawvpvmfdfyvrlymwvlgawdlivqihmjdar
ivnjglahitlnvydtpwilrltrawvuvyvefarheawvolu
dhyafyjvahhjabvfnwlytifrx

mjifynlsfrfaahawvlrailcvyvdlplyjeldajifynqglyafy
plddgvrefvgmdwvrwfivpiohwyrhymvevymvmfarslddfyv

hyawvhkehimlrailcvyvdlslddfyvawvuvraawfynqvhqgv
rwhjgmmhfrghhxlabwlaawvpwilrlthjifymvqvymvya
ivnjglahiawlarbwtbvwlsvawvpawlarbwtawvtliv
fymvqvymvyarlfmpiohwyrhybwhwlrivdvfsvmawvefira
mhrvheawvslddfyvwfprvge

awvfilmsfdvahqvhqgvfrahxvvqnhfynhjaawvivnva
thjiolunvathjirvdhymolu

awvqifpvpfyfravilmmvmawvuvraawfynhelggfrah
slddfylavhjiqhqjglafhynvavsvituhmthjanvaafynawv
oluawlarawvxvtawfynlymawlarbwlafbhjgm
lmshdlavyjpuvihyv
	"""
	test_substitutions(enc_text,True)
	for phrase in probable_phrases:
		definitive_phrases[phrase] = 0
		for w in italiano:
			if w in phrase:
				definitive_phrases[phrase] += 1
		for w in inglese:
			if w in phrase:
				definitive_phrases[phrase] += 1
	definitive_phrases_ordered = list(order_dict(definitive_phrases).keys())
	print("")
	print("")
	print("")
	print("")
	print("")
	print("")
	print(colored(definitive_phrases_ordered[0],"green"))
	print("")


if __name__ == '__main__':
	main()














