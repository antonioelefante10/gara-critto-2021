import math
import re
from threading import Thread
from datetime import datetime
import os

os.system("clear")
now = datetime.now()


def text_formatter(text):

    symbol_less_text = re.sub(r"[^A-Za-z]+", '', text)
    uppercase_text = symbol_less_text.upper()
    return uppercase_text

ALFABETO_CIFRANTE = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"

IS_ENGLISH = True
probable_result = {}
text = """eocsullfpgooadvhnelfdilssgiaynenrgfibmraooaltevtncaiacrrtpeosrpeaa
osdefoxetofhsdoatrnzreeavccoiacadcivecnrdenmioithlrbuuaainfyteptyg
dmiimnscaherlrvaeeeouparnonutcpersaideudisbrsiutootnfioetcvhcviacd
aeimdnhyetebeiuvrnotsyfirfoodxdauntkaphhreucmetafailictarsrmeeancz
loalofrnwgeisrpttoothsmapeeolpdaedehplvoeeoldodbtoctilsdniogserrad
refretiicvnejhgeatdhbeetoscinieesrbwoeadnsuraopgoaoruflmnildanivco
tncaeipedolphneteieiutdndnkgoienmduaaprenoniuoonmhfoewiexrepsendec
sorubicoldlossoasttioaechidtlweeordweaptlluceontsrteusgilnittenahd
eoswvheaervrfwioteschaeehystebpreoeudmaeinsnceaiyngceeucmolasodtan
ieyfndeiivltthyehwetehrrtoerepeadssclrweietendoksetathnzreeacvcaca
neiadnlnccuohddtaebhteettfniseetohvfncaiecetowiuyagnrhhkitestucnro
asihvecienesmsrueecadcivotninahlstoaehugrghnmyaaptsopsvgeiidetnhvg
ncaiecotthsoenedrurtaeifnotwssyeatmfoiotrnygissnrmtseeerptdorvseee
doboclitlsnouraopglmoilfanivcotncaeipedolpzzezzz"""
text = text_formatter(text)

common_italian_words = []
common_english_words = []
min_word_lenght = 4
max_word_lenght = 10
possible_plain_text = []
permutations = []
depth = 9
maximum_score = 0
threads = []


########################################################
######## LETTURA PER RIGA E SCRITTURA PER RIGA #########
########################################################

def enc_message_read_r_write_r(plaintext,key):
    ciphertext = ""
    for index in range(len(plaintext)):
        key_idx = index % len(key)
        multiplier = math.floor(index/len(key))
        curr_key = ALFABETO_CIFRANTE.index(key[key_idx])
        ciphertext = ciphertext + plaintext[curr_key + (len(key) * multiplier)]
    return ciphertext

def dec_message_read_r_write_r(ciphertext,key):
    inv_key = inverse_key(key)
    return enc_message_read_r_write_r(ciphertext,inv_key)
    


########################################################
######## LETTURA PER RIGA E SCRITTURA PER COLONNA ######
########################################################
def enc_message_read_r_write_c(plaintext,key):
    ciphertext_r_by_r = enc_message_read_r_write_r(plaintext,key)
    # inizializzo il ciphertext in scrittura per colonna mediante
    # il testo cifrato per riga così da avere accesso al singolo
    # carattere mediante indice
    ciphertext_column = list(ciphertext_r_by_r)
    column_increment = math.floor(len(ciphertext_column) / len(key))
    j = 0
    for index in range(len(ciphertext_r_by_r)):
        key_idx = index % len(key)
        access_index = key_idx * column_increment + j
        ciphertext_column[access_index] = ciphertext_r_by_r[index]
        if(index%len(key) == len(key)-1):
            j += 1
    return "".join(ciphertext_column)

def dec_message_read_r_write_c(ciphertext,key):
    inv_key = inverse_key(key)
    ciphertext_column = list(ciphertext)
    column_increment = math.floor(len(ciphertext_column) / len(key))
    j = 0
    for index in range(len(ciphertext)):
        key_idx = index % len(key)
        access_index = key_idx * column_increment + j
        ciphertext_column[index] = ciphertext[access_index]
        if(index%len(key) == len(key)-1):
            j += 1
    ciphertext_column = "".join(ciphertext_column)
    return enc_message_read_r_write_r(ciphertext_column,inv_key)
    

########################################################
######## LETTURA PER COLONNA E SCRITTURA PER RIGA ######
########################################################
def enc_message_read_c_write_r(plaintext,key):
    key = inverse_key(key)
    plaintext_column = list(plaintext)
    column_increment = math.floor(len(plaintext_column) / len(key))
    j = 0
    for index in range(len(plaintext)):
        key_idx = index % len(key)
        access_index = key_idx * column_increment + j
        plaintext_column[index] = plaintext[access_index]
        if(index%len(key) == len(key)-1):
            j += 1
    plaintext_column = "".join(plaintext_column)
    return enc_message_read_r_write_r(plaintext_column,key)

def dec_message_read_c_write_r(ciphertext,key):
    inv_key = inverse_key(key)
    plaintext = dec_message_read_r_write_r(ciphertext,inv_key)
    plaintext_column = list(plaintext)
    column_increment = math.floor(len(plaintext_column) / len(key))
    j = 0
    for index in range(len(plaintext)):
        key_idx = index % len(key)
        access_index = key_idx * column_increment + j
        plaintext_column[access_index] = plaintext[index]
        if(index%len(key) == len(key)-1):
            j += 1
    plaintext_column = "".join(plaintext_column)
    return plaintext_column

def enc_message_read_c_write_c(plaintext,key):
    column_increment = math.floor(len(plaintext)/len(key))
    plaintext_column = list(plaintext)
    for index in range(len(key)):
        key_idx = index % len(key)
        for j in range(column_increment):
            curr_key = key.index(ALFABETO_CIFRANTE[key_idx])
            access_index = key_idx * column_increment + j
            key_access = curr_key * column_increment + j
            plaintext_column[access_index] = plaintext[key_access]
    encoded = "".join(plaintext_column)
    return encoded

def dec_message_read_c_write_c(ciphertext,key):
    return enc_message_read_c_write_c(ciphertext,inverse_key(key))


def inverse_key(key):
    inverse_key = ""
    for i in range(len(key)):
        key_idx = key.index(ALFABETO_CIFRANTE[i])
        inverse_key += ALFABETO_CIFRANTE[key_idx]
    return inverse_key


def permute(a, l, r):
    global permutations
    if l==r:
        permutations.append("".join(a))
    else:
        for i in range(l,r+1):
            a[l], a[i] = a[i], a[l]
            permute(a, l+1, r)
            a[l], a[i] = a[i], a[l]


def check_words_in_decoded(decoded, key):
    global probable_result
    global maximum_score
    multiplier = 1
    current_sum = 1
    if IS_ENGLISH:
        for w in common_english_words:
            if w in decoded:
                current_sum += 1 * multiplier
                multiplier += 1
    else:
        for w in common_italian_words:
            if w in decoded:
                current_sum += 1 * multiplier
                multiplier += 1
    probable_result[decoded] = current_sum
    if current_sum > maximum_score:
        maximum_score = current_sum
        print("")
        print("score value: " + str(current_sum))
        print("key: " + key)
        print(decoded)
        print("")
        print("")
        later = datetime.now()
        difference = (later - now).total_seconds()
        print("time elapsed in seconds: " + str(difference))
        print("")
        print("")
    return current_sum

def try_all_permutations(ciphertext,key):
    global possible_plain_text
    while True:
        if len(ciphertext) % len(key) == 0:
            break
        ciphertext = ciphertext[0:-1]
    decoded1 = dec_message_read_r_write_r(ciphertext,key)
    check_words_in_decoded(decoded1,key)
    decoded2 = dec_message_read_r_write_c(ciphertext,key)
    check_words_in_decoded(decoded2,key)
    decoded3 = dec_message_read_c_write_r(ciphertext,key)
    check_words_in_decoded(decoded3,key)
    decoded4 = dec_message_read_c_write_c(ciphertext,key)
    check_words_in_decoded(decoded4,key)
    possible_plain_text.append(decoded1)
    possible_plain_text.append(decoded2)
    possible_plain_text.append(decoded3)
    possible_plain_text.append(decoded4)


if IS_ENGLISH:
    with open('./Dizionari/common_english.txt', encoding='utf8') as f:
        for line in f :
            w = line.strip().upper()
            if len(w) > min_word_lenght and len(w) < max_word_lenght:
                common_english_words.append(w)
else:
    with open('./Dizionari/common_italian.txt', encoding='utf8') as f:
        for line in f:
            w = line.strip().upper()
            if len(w) > min_word_lenght and len(w) < max_word_lenght:
                common_italian_words.append(w)


for i in range(3,depth):
    combinations = ALFABETO_CIFRANTE[0:i]
    n = len(combinations)
    a = list(combinations)
    permute(a, 0, n-1)
    for combination in permutations:
        try_all_permutations(text,combination)
        #thread = Thread(target=try_all_permutations,args=(text,combination))
        #thread.start()
        #threads.append(thread)
#for thread in threads:
#    thread.join()

probable_result = {k: v for k, v in sorted(probable_result.items(), key=lambda item: item[1])}

for res in probable_result:
    print(res)
    print(" ")
    print(" ")

later = datetime.now()
difference = (later - now).total_seconds()
print("time elapsed in seconds: " + str(difference))


