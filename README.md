### Tool utilizzati per la Gara di Crittografia 20/21



L'utilizzo di tale software è relativamente semplice in quanto è configurato affinchè possa essere eseguito il file in maniera diretta e senza la necessità di effettuare particolari modifiche.

Infatti è facilmente osservabile che il file ciphertext1.py ed il ciphertext4.py sono completamente identici a meno del testo cifrato che è stato inserito in maniera cable-coded così da semplificare ulteriormente l'esecuzione.

Sono state utilizzate solo le librerie strettamente necessarie più la libreria https://pypi.org/project/termcolor/ la quale permette di personalizzare il colore del testo mostrato a video.





In conclusione, ringraziamo il prof. Cimato per aver organizzato tale competizione rompendo difatti la monotonia universitaria e proponendo qualcosa di semplice ed innovativo che ha rinnovato in noi il desiderio di imparare.



La competizione è il principale vettore di crescita e miglioramento

Gruppo 3:
    Antonio Elefante
    Salvatore Ambrosio
    Alfonso D'Auria
